package Model;

public abstract class Conta {
	protected double saldo;

	public abstract void atualiza(double taxa);

	public void deposita(double valor) {
		this.saldo += valor;

	}

	public double getSaldo() {
		// TODO Auto-generated method stub
		return this.saldo;
	}
}
